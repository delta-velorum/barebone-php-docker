### Running a basic PHP application in Docker
This repository is for educational purposes, and should probably not be used in a production envirnoment

### Building the container
May take a while
```
docker build -t barebone-php-docker .
```

### Starting the container
```
docker run -p 80:80 barebone-php-docker
```

Available at http://localhost